﻿using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Business.UserBusiness
{
    public class UserBusiness
    {
       
        //private string _usersFilePath = "/Files/Users.txt";
        
        private User _user;
        public UserBusiness(User user)
        {
            _user = user;           
        }
        public User AddUser()
        {
            _user.Money = SetGifPromotionAmount();
            _user.Email = NormalizeEmail();

            if (IsADuplicatedUser())
            {
                _user.Result.IsSuccess = false;
                _user.Result.Errors = "The user is duplicated";
            }
            else
            {
                _user.Result.IsSuccess = true;
                _user.Result.Errors = "User Created";
            }
            return _user;
        }

        public decimal  SetGifPromotionAmount()
        {
            decimal gifPromotion = _user.Money;
            switch (_user.UserType)
            {
                case UserType.Normal:
                    gifPromotion = GetGifPromotionForNormalTypeOfUser();
                    break;
                case UserType.SuperUser:
                    gifPromotion = GetGifPromotionForSuperUser();
                    break;
                case UserType.Premium:
                    gifPromotion = GetGifPromotionForPremimUser();
                    break;
                default:
                    gifPromotion = _user.Money;
                    break;
            }
            return gifPromotion;

        }
        private decimal GetGifPromotionForNormalTypeOfUser()
        {
            decimal gifPromotion = _user.Money;
            if (_user.Money > 100)
            {
                var gif = CalculateGiftAmount(Convert.ToDecimal(0.12), _user.Money);
                gifPromotion = _user.Money + gif;
            }
            if ((_user.Money < 100) && (_user.Money > 10))
            {
                var gif = CalculateGiftAmount(Convert.ToDecimal(0.8), _user.Money);
                gifPromotion = _user.Money + gif;
            }

            return gifPromotion;
        }
        private decimal GetGifPromotionForPremimUser()
        {
            decimal gifPromotion = _user.Money;
            if (_user.Money > 100)
            {
                var gif = CalculateGiftAmount(2, _user.Money);
                gifPromotion = _user.Money + gif;
            }

            return gifPromotion;
        }

        private decimal GetGifPromotionForSuperUser()
        {
            decimal gifPromotion = _user.Money;
            if (_user.Money > 100)
            {
                var gif = CalculateGiftAmount(Convert.ToDecimal(0.20), _user.Money);
                gifPromotion = _user.Money + gif;
            }

            return gifPromotion;
        }

        private decimal CalculateGiftAmount(decimal percentage, decimal money)
        {
            decimal gif = 0;           
            gif = money * percentage;
            return gif;
        }
        public string NormalizeEmail()
        {
            string normalizeEmail = string.Empty;
            var aux = _user.Email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);

            var atIndex = aux[0].IndexOf("+", StringComparison.Ordinal);

            aux[0] = atIndex < 0 ? aux[0].Replace(".", "") : aux[0].Replace(".", "").Remove(atIndex);

            normalizeEmail = string.Join("@", new string[] { aux[0], aux[1] });
            return normalizeEmail;
        }
       
        
        public bool IsADuplicatedUser()
        {            
            bool isDuplicated = false;
            var reader = ReadUsersFromFile();
            List<User> usersFromFile = GetUserFromReader(reader);
            
             isDuplicated = usersFromFile
                .Any(x => x.Email.Equals(_user.Email) || x.Phone.Equals(_user.Phone) 
                || (x.Name.Equals(_user.Name) && x.Address.Equals(_user.Address)));
            
            return isDuplicated;
        }

        private StreamReader ReadUsersFromFile()
        {
            var path = Directory.GetCurrentDirectory() + _usersFilePath;

            FileStream fileStream = new FileStream(path, FileMode.Open);

            StreamReader reader = new StreamReader(fileStream);
            return reader;
        }
        private List<User> GetUserFromReader(StreamReader reader)
        {
            List<User> users = new List<User>();
            while (reader.Peek() >= 0)
            {
                var line = reader.ReadLineAsync().Result;

                var name = line.Split(',')[0].ToString();
                var email = line.Split(',')[1].ToString();
                var phone = line.Split(',')[2].ToString();
                var address = line.Split(',')[3].ToString();
                var userType = line.Split(',')[4].ToString();
                var money = decimal.Parse(line.Split(',')[5].ToString());
                User user = new User(name, email, address, phone, userType, money);

                users.Add(user);
            }
            reader.Close();
            return users;
        }
    }
}
