using Moq;
using Sat.Recruitment.Api.Business;
using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.Entities;
using Sat.Recruitment.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Sat.Recruitment.Test
{
    [CollectionDefinition("Tests", DisableParallelization = true)]
    public class UserBusinessTest
    {
        private readonly Mock<IUserBusiness> _mockBusiness;
        private readonly Mock<IUserRepository> _repository;       
        public UserBusinessTest()
        {
            _repository = new Mock<IUserRepository>();
            _mockBusiness = new Mock<IUserBusiness>();
           
        }
        [Fact]
        public void GetUser_should_return_user()
        {
            List<User> users = new List<User>();
            User userToReturn = new User("Jose", "jose@gmail.com", "calle siempre viva", "55555", "Normal", 100);
            
            userToReturn.Id = 2;
           
            users.Add(userToReturn);
            long userId = 2;

            UserBusiness userBusiness = new UserBusiness(_repository.Object);
            
            _repository.Setup(m => m.GetUser(It.IsAny<long>())).Returns(userToReturn);
            var user = userBusiness.GetUser(userId);

                

            Assert.True(userToReturn.Id == 2);

        }

        [Fact]
        public void AddUser_should_add_a_new_user_to_a_list_of_users()
        {
          
            UserBusiness userBusiness = new UserBusiness(_repository.Object);
            var userToAdd = new User("miguel", "jose@gmail.com", "calle siempre viva", "55555", "Normal", 100);
            
            List<User> users = new List<User>() 
            {
                new User("Jose", "jose@gmail.com", "calle siempre viva", "55555", "Normal", 110),
                new User("miguel", "jose@gmail.com", "calle siempre viva", "55555", "Normal", 100)
            };

            _repository.Setup(x => x.AddUser(It.IsAny<User>())).Returns(users);

            var userReturned = userBusiness.AddUser(userToAdd);

            Assert.NotNull(userReturned);
            Assert.True(userReturned.Money > 100);
            
        }

        

        
    }
}
