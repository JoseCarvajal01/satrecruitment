﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Sat.Recruitment.Api.Business;
using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.Controllers;
using Sat.Recruitment.Api.Entities;
using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Services;
using System.Collections.Generic;
using System.Net.Http;
using Xunit;

namespace Sat.Recruitment.Test.Controller
{
    
    public class UserControllerShould
    {
        private readonly Mock<IUserBusiness> _mockBusiness;
        private readonly Mock<IMapper> _mapper;
        private readonly UsersController _controller;
        public UserControllerShould()
        {
            _mockBusiness = new Mock<IUserBusiness>();
            _mapper = new Mock<IMapper>();
            _controller = new UsersController(_mockBusiness.Object, _mapper.Object);
        }
        [Fact]
        public void GetUser_Should_return_status_code_200()
        {
            
            User user = new User("Jose", "jose@gmail.com", "calle siempre viva", "55555", "Normal", 100);
            user.Id = 2;
            var userId = 2;
            _mockBusiness.Setup(m => m.GetUser(It.IsAny<long>())).Returns(user);
            IActionResult result = _controller.GetUser(userId);
           
            Assert.IsType<OkObjectResult>(result);

        }

        
        [Fact]
        public void CreateUser_Should_return_CreatedAtRoute_statusCode()
        {
            User user = new User("Jose", "jose@gmail.com", "calle siempre viva", "55555", "Normal", 100);
            UserDTO userDTO = new UserDTO()
            {
                Id = 1,
                Name = "jose",
                Address = "calle siempre viva",
                Email = "jose@gmail.com",
                UserType = "Normal",
                Phone = "5555",
                Money = "134"
            };
            UserForCreationDTO userForCreationDTO = new UserForCreationDTO()
            {                
                Name = "jose",
                Address = "calle siempre viva",
                Email = "jose@gmail.com",
                UserType = "Normal",
                Phone = "5555",
                Money = "100"
            };
            _mockBusiness.Setup(m => m.AddUser(It.IsAny<User>()));
            _mapper.Setup(x => x.Map<User>(It.IsAny<UserForCreationDTO>())).Returns(user);
            _mapper.Setup(x => x.Map<UserDTO>(It.IsAny<User>())).Returns(userDTO);




            var result = _controller.CreateUser(user: userForCreationDTO);
            Assert.IsType<CreatedAtRouteResult>(result);

        }
    }
}
