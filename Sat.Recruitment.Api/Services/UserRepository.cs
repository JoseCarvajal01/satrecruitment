﻿using Sat.Recruitment.Api.Business;
using Sat.Recruitment.Api.DataAceess;
using Sat.Recruitment.Api.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Sat.Recruitment.Api.Services
{
    public class UserRepository : IUserRepository
    {
        
        private UserDataAccess _userDataAccess;
        //private IUserBusiness _userBusiness;


        public UserRepository(UserDataAccess userDataAccess )
        {
            _userDataAccess = userDataAccess;
            //_userBusiness = userBusiness;
        }
        public User GetUser(long userId)
        {
            return _userDataAccess.
                UsersData.
                Find(x => x.Id == userId);
        }

        public List<User> AddUser(User userEntity)
        {
            //var userResult = _userBusiness.AddUser(userEntity);
            userEntity.Id = _userDataAccess.UsersData.OrderBy(x => x.Id).Last().Id + 1;
            
            if (IsADuplicatedUser(userEntity))
            {
                userEntity.Result.IsSuccess = false;
                userEntity.Result.Errors = "The user is duplicated";
            }
            else
            {
                userEntity.Result.IsSuccess = true;
                userEntity.Result.Errors = "User Created";
            }
           
            _userDataAccess.UsersData.Add(userEntity);
            return _userDataAccess.UsersData;
        }
        public bool IsADuplicatedUser(User user)
        {
            bool isDuplicated = false;

            isDuplicated = _userDataAccess.UsersData
               .Any(x => x.Email.Equals(user.Email) || x.Phone.Equals(user.Phone)
               || (x.Name.Equals(user.Name) && x.Address.Equals(user.Address)));

            return isDuplicated;
        }
    }
}
