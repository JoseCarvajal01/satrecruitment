﻿using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.Entities;
using System.Collections.Generic;

namespace Sat.Recruitment.Api.Services
{
    public interface IUserRepository
    {        
        List<User> AddUser(User userEntity);
        User GetUser(long userId);        
        bool IsADuplicatedUser(User user);
    }
}
