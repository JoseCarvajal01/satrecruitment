﻿using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Entities
{
    public class User : AggregateRoot
    {
        [Required(ErrorMessage = "The name is required")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "The email is required")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = " The address is required")]
        public string Address { get; set; }
        
        [Required(ErrorMessage = "The phone is required")]
        public string Phone { get; set; }
        public  UserType UserType{ get; set; }
        public decimal Money { get; set; }
        public Result Result { get; set; }

        public User(string name, string email, string address, string phone, string userType,decimal money)
        {
            Name = name;
            Email = email;
            Address = address;
            Phone = phone;
            UserType = (UserType)Enum.Parse(typeof(UserType), userType);           
            Money = money;
            Result = new Result();
        }       
        
    }
}
