﻿using AutoMapper;
using Sat.Recruitment.Api.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.DataAceess
{
    public class UserDataAccess 
    {
        private string _usersFilePath = "/Files/Users.txt";
        public List<User> UsersData { get; set; }
        
        public UserDataAccess() 
        {
            UsersData = GetUserReferenceData();
        }
        private List<User> GetUserReferenceData()
        {
            var reader = ReadUsersFromFile();
            List<User> usersFromFile = GetUserFromReader(reader);
            return usersFromFile;

        }
        private StreamReader ReadUsersFromFile()
        {
            var path = Directory.GetCurrentDirectory() + _usersFilePath;

            FileStream fileStream = new FileStream(path, FileMode.Open);

            StreamReader reader = new StreamReader(fileStream);
            return reader;
        }
        private List<User> GetUserFromReader(StreamReader reader)
        {
            long counter = 0;
            List<User> users = new List<User>();
            while (reader.Peek() >= 0)
            {
                
                var line = reader.ReadLineAsync().Result;

                var name = line.Split(',')[0].ToString();
                var email = line.Split(',')[1].ToString();
                var phone = line.Split(',')[2].ToString();
                var address = line.Split(',')[3].ToString();
                var userType = line.Split(',')[4].ToString();
                var money = decimal.Parse(line.Split(',')[5].ToString());

                User user = new User(name, email, address, phone, userType, money);
                
                users.Add(user);
                user.Id = counter;
                counter++;
            }
            reader.Close();
            return users;
        }
    }
}
