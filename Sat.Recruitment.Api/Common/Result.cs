﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Common
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Errors { get; set; }
        
    }
}
