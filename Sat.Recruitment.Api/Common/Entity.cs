﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Common
{
    public abstract class Entity
    {
        public virtual long Id { get;  set; }
    }
}
