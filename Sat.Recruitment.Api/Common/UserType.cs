﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Common
{
    public enum UserType
    {
        Normal = 0,
        SuperUser = 1,
        Premium = 2,
    }
}
