﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Sat.Recruitment.Api.Business;
using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.DataAceess;
using Sat.Recruitment.Api.Entities;
using Sat.Recruitment.Api.Models;
using Sat.Recruitment.Api.Services;
using System;

namespace Sat.Recruitment.Api.Controllers
{

    [ApiController]
    [Route("api/user")]
    public partial class UsersController : ControllerBase
    {

        
        private IMapper _mapper;
        private IUserBusiness _userBusiness;
        public UsersController(IUserBusiness userBusiness, IMapper mapper)
        {
            _userBusiness = userBusiness ?? throw new ArgumentNullException(nameof(userBusiness));            
            _mapper = mapper;
        }
        
        [HttpGet("{userId}", Name ="GetUser")]
        public IActionResult GetUser(long userId)
        {
            var userFromRepo = _userBusiness.GetUser(userId);

            if (userFromRepo == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<UserDTO>(userFromRepo));
        }
        [HttpPost]        
        public IActionResult CreateUser([FromBody]UserForCreationDTO user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userEntity = _mapper.Map<User>(user);

            _userBusiness.AddUser(userEntity);
            
            var userToReturn = _mapper.Map<UserDTO>(userEntity);
            return CreatedAtRoute("GetUser", new { userId = userToReturn.Id },userToReturn);      
            
        }        
        
    }   
}
