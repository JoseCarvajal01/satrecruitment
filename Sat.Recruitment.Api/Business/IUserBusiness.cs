﻿using Sat.Recruitment.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sat.Recruitment.Api.Business
{
    public interface IUserBusiness
    {
        User AddUser(User user);
        User GetUser(long userId);


    }
}
