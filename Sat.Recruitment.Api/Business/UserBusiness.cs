﻿using Sat.Recruitment.Api.Common;
using Sat.Recruitment.Api.DataAceess;
using Sat.Recruitment.Api.Entities;
using Sat.Recruitment.Api.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Sat.Recruitment.Api.Business
{
    public class UserBusiness : IUserBusiness
    {
        private IUserRepository _userRepository;
        public UserBusiness(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public User GetUser(long userId)
        {
            return _userRepository.GetUser(userId);
        }
        public User AddUser(User user)
        {

            user.Money = SetGifPromotionAmount(user.UserType, user.Money);
            user.Email = NormalizeEmail(user.Email);

           var resultinData = _userRepository.AddUser(user);
            
            return resultinData.Find(x => x.Id == user.Id);
        }

        public decimal  SetGifPromotionAmount(UserType userType, decimal money)
        {
            decimal gifPromotion = money;
            switch (userType)
            {
                case UserType.Normal:
                    gifPromotion = GetGifPromotionForNormalTypeOfUser(money);
                    break;
                case UserType.SuperUser:
                    gifPromotion = GetGifPromotionForSuperUser(money);
                    break;
                case UserType.Premium:
                    gifPromotion = GetGifPromotionForPremimUser(money);
                    break;
                default:
                    gifPromotion = money;
                    break;
            }
            return gifPromotion;

        }
        private decimal GetGifPromotionForNormalTypeOfUser(decimal money)
        {
            decimal gifPromotion = money;
            if (money > 100)
            {
                var gif = CalculateGiftAmount(Convert.ToDecimal(0.12), money);
                gifPromotion = money + gif;
            }
            if ((money < 100) && (money > 10))
            {
                var gif = CalculateGiftAmount(Convert.ToDecimal(0.8), money);
                gifPromotion = money + gif;
            }

            return gifPromotion;
        }
        private decimal GetGifPromotionForPremimUser(decimal money)
        {
            decimal gifPromotion = money;
            if (money > 100)
            {
                var gif = CalculateGiftAmount(2, money);
                gifPromotion = money + gif;
            }

            return gifPromotion;
        }

        private decimal GetGifPromotionForSuperUser(decimal money)
        {
            decimal gifPromotion = money;
            if (money > 100)
            {
                var gif = CalculateGiftAmount(Convert.ToDecimal(0.20), money);
                gifPromotion = money + gif;
            }

            return gifPromotion;
        }

        private decimal CalculateGiftAmount(decimal percentage, decimal money)
        {
            decimal gif = 0;           
            gif = money * percentage;
            return gif;
        }
        public string NormalizeEmail(string email)
        {
            string normalizeEmail = string.Empty;
            var aux = email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);

            var atIndex = aux[0].IndexOf("+", StringComparison.Ordinal);

            aux[0] = atIndex < 0 ? aux[0].Replace(".", "") : aux[0].Replace(".", "").Remove(atIndex);

            normalizeEmail = string.Join("@", new string[] { aux[0], aux[1] });
            return normalizeEmail;
        }

        
    }
}
